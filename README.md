# README
# currency-changes-web

> Make sure you have [node JS](https://nodejs.org/en/download/) installed on your local machine.

## Installation

1. Clone the project ```git clone https://e2businesslt@bitbucket.org/e2businesslt/currency-changes-web.git```.
2. Install required packages by executing ```npm install``` command in your command line interface (you must be in project's root folder).
3. Run ```npm start``` for running application in development mode.

## Running application in production

1. Execute ```npm run build``` in your command line interface.

> Application is running on [http://localhost:3000](http://localhost:3000). 
