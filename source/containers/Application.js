import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../actions/actions';
import RatesTable from '../components/RatesTable';
import Message from '../components/Message';

class Application extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            date: '',
            canFetch: true
        }
    }

    changeValue = (event) => {
        this.setState({
            date: event.target.value
        });
    }

    checkValue = (value) => {
        return /(\d{4}-\d{2}|\d{1}-\d{2}|\d{1})/.test(value) || /(\d{4}\.\d{2}|\d{1}\.\d{2}|\d{1})/.test(value) ||
            /(\d{4} \d{2}|\d{1} \d{2}|\d{1})/.test(value) || /(\d{4}\,\d{2}|\d{1}\,\d{2}|\d{1})/.test(value);
    }

    onSubmit = () => {
        let canFetch = this.checkValue(this.state.date);
        this.setState({
            canFetch
        });

        canFetch ? this.fetchRates() : this.clearRates();
    }

    clearRates = () => {
        this.props.actions.clearRates();
    }

    fetchRates = () => {
        this.props.actions.fetchRates(this.state.date)
    }

    render() {
        const { rates } = this.props;
        const { responseMessage } = this.props;
        const { errorInResponse } = this.props;
        const cannotFetch = !this.state.canFetch;
        return (
            <div>
                <div>
                    <input
                        type='text' 
                        value={ this.state.date } 
                        onChange={ this.changeValue }
                        style={{ marginRight: 16 }}/>
                    <button 
                        type='button'
                        onClick={ this.onSubmit }>
                        { 'Fetch currency rates' }
                    </button>
                </div>
                {
                    (responseMessage || cannotFetch) ?
                        <div style={{ paddingTop: 16 }}>
                            <Message 
                                message={ cannotFetch ? 'Invalid date format.' : responseMessage }
                                error={ errorInResponse || cannotFetch } />
                        </div> : ''
                }
                {
                    rates && rates.length > 0 ?
                        <div style={{ paddingTop: 16 }}>
                            <RatesTable rates={ rates } />
                        </div> : ''
                }
            </div>
        );
    }
}

function mapStateToProps(state, props) {
	return {
        rates: state.currencyRatesReducer.rates,
        responseMessage: state.currencyRatesReducer.responseMessage,
        errorInResponse: state.currencyRatesReducer.errorInResponse
	}
}

function mapActionsToProps(dispatch) {
	return {
		actions: bindActionCreators(actions, dispatch)
	}
}

export default connect(mapStateToProps, mapActionsToProps)(Application);
