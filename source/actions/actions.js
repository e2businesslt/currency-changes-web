import * as actionTypes from './actionTypes';
import axios from 'axios';

const api = 'http://localhost:9000';

export const fetchRatesSuccess = (data) => ({
    type: actionTypes.FETCH_RATES_FOR_DATE_SUCCESS,
    data
});

export const fetchRatesError = (data) => ({
    type: actionTypes.FETCH_RATES_FOR_DATE_ERROR,
    data
});

export const fetchRates = (date) => {
    return (dispatch) => {
        return axios
            .post(`${api}/exchanges`, { date })
            .then(response => { 
                try {
                    dispatch(fetchRatesSuccess(response.data))
                } catch(error) {
                    console.log(error);
                    dispatch(fetchRatesError(response.data))
                }
             })
            .catch(error => { throw(error); })
    };
};

export const clearRates = () => ({
    type: actionTypes.CLEAR_RATES
});
