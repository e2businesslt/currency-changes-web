import * as actionTypes from '../actions/actionTypes';

const initialState = {
    rates: [],
    responseMessage: undefined,
    errorInResponse: false
};

const reducer = (state = initialState, action) => {
    const { type } = action;

    if (type === actionTypes.FETCH_RATES_FOR_DATE_SUCCESS) {
        return {
            ...state,
            responseMessage: action.data.description,
            rates: action.data.rates.currencyArray,
            errorInResponse: false
        }
    }

    if (type === actionTypes.FETCH_RATES_FOR_DATE_ERROR) {
        return {
            ...state,
            responseMessage: action.data.description,
            errorInResponse: true
        }
    }

    if (type === actionTypes.CLEAR_RATES) {
        return {
            ...state,
            rates: []
        }
    }

    return state;
};

export default reducer;
