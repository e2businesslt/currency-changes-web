import { combineReducers } from 'redux';
import currencyRatesReducer from './currencyRatesReducer';

export default combineReducers({
    currencyRatesReducer
});