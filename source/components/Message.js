import React from 'react';

const Message = ({ message, error }) => (
    <div style={{ color: error ? '#F44336' : '#4CAF50' }}>
        { message }
    </div>
);

export default Message;