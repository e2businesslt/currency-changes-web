import React from 'react';

const RatesTable = ({ rates }) => (
    <table border="1">
        <thead>
            <tr>
                <th>currency</th>
                <th>quantity</th>
                <th>rate</th>
                <th>unit</th>
                <th>date</th>
            </tr>
        </thead>
        <tbody>
            { rates.map((entry, index) => (
                <tr key={ index }>
                    <td>{ entry.currency }</td>
                    <td>{ entry.quantity }</td>
                    <td>{ entry.rate }</td>
                    <td>{ entry.unit }</td>
                    <td>{ entry.date }</td>
                </tr>
            )) }
        </tbody>
    </table>
);

export default RatesTable;