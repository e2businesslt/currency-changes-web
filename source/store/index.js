import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducer from '../reducers';

const configureStore = (state) => {
    return createStore(reducer, state, applyMiddleware(thunk));
};

export default configureStore;