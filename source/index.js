import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import Application from './containers/Application';
import configureStore from './store';
import { Provider } from 'react-redux';

import './resources/stylesheets/style.css';

const store = configureStore();

const doRender = (ComponentToRender) => {
    render(
        <AppContainer>
            <Provider store={ store }>
                <ComponentToRender/>
            </Provider>
        </AppContainer>,
        document.getElementById("mount")
    );
};

doRender(Application);

if (module.hot) {
    module.hot.accept('./containers/Application', () => {
        const toRender = require('./containers/Application').default;
        doRender(toRender);
    });
}