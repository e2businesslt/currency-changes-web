import express from 'express';
import { join } from 'path';
import open from 'open';
import compression from 'compression';

const port = process.env.PORT || 3000;
const app = express();

app.use(compression());
app.use(express.static(join(__dirname, '../output')));

app.get('*', (request, response) => {
    response.sendFile(join(__dirname, '../output/index.html'));
});

app.listen(port, (error) => {
    error ? console.log(error) : open(`http://localhost:${port}`);
});