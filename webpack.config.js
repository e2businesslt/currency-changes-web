const { join, resolve } = require('path');
const ExtractText = require('extract-text-webpack-plugin');
const HyperTextMarkup = require('html-webpack-plugin');
const Md5Hash = require('webpack-md5-hash');
const webpack = require('webpack');

const port = process.env.PORT || 3000;
const environment = process.env.NODE_ENV || 'development';
const production = environment === 'production';
const entry = join(__dirname, './source/index.js');
const configuration = {
    mode: environment,
    devtool: production ? undefined : 'inline-source-map',
    entry: production ? { application: entry } : [
        'react-hot-loader/patch', 
        'webpack-dev-server/client?http://localhost:3000', 
        'webpack/hot/only-dev-server', 
        entry
    ],
    output: {
        path: resolve(__dirname, 'output'),
        filename: production ? '[name].[chunkhash].js' : 'application.js'
    },
    target: 'web',
    module: {
        rules: [
            {
                test: /(\.js|\.jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.css$/,
                use: production ? ExtractText.extract({
                    fallback: 'style-loader',
                    use: ['css-loader']
                }) : [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' }
                ]
            }
        ]
    },
    plugins: production ? [
        new Md5Hash(),
        new ExtractText({
            filename: 'style.[hash].css',
            disable: false,
            allChunks: true
        }),
        new HyperTextMarkup({
            template: join(__dirname, './source/index.html'),
            filename: 'index.html',
            hash: true,
            inject: false,
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                minifyCSS: true,
                minifyJS: true
            }
        })
    ] : [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new HyperTextMarkup({
            template: join(__dirname, './source/index.html'),
            filename: 'index.html',
            inject: true
        })
    ],
    devServer: production ? undefined : {
        host: 'localhost',
        port: port,
        hot: true,
        inline: true
    }
};

module.exports = configuration;
